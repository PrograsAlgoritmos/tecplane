package Clases;

public class Nodo<T> {
	
	//atributos
	
	private T element;
	private Nodo<T> next;
	
	//Constructores
	public Nodo() {
		this.element = null;
		this.next = null;
	}
	
	public Nodo(T element) {
		this.element = element;
		this.next = null;
	}
	
	public Nodo(T element, Nodo<T> next) {
		this.element = element;
		this.next = next;
	}
	
	//metodos
	
	public T getElement() {
		return this.element;
	}
	
	public void setElement(T element) {
		this.element = element;
	}
	
	public Nodo<T> getNext() {
		return this.next;
	}
	
	public void setNext(Nodo<T> next) {
		this.next = next;	
	}
}
