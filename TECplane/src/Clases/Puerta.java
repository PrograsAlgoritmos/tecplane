package Clases;

public class Puerta {
	
	//atributos de las puertas. Estas tienen un vuelo que se les asigna y un codigo numerico//atributos de las puertas. Estas tienen un vuelo que se les asigna y un codigo numerico
	private Vuelo vuelo;
	private int codigo;
	private static int atendidos;
	private String estado;
	
	//crea una puertas dependiendo de la cantidad de puertas creadas
	public Puerta (int codigo) {
		this.codigo = codigo;
		this.atendidos = 0;
		this.estado = "cerrado";
	}
	
	//modifica el vuelo asignado
	public void setVuelo(Vuelo vuelo) {
		this.vuelo = vuelo;
		this.estado = "abierto";
	}
	
	//modificar estado
	public void cerrar() {
		this.estado = "cerrado";
	}
	
	//aumenta el numero de atendidos en la puerta
	public void Atendidos() {
		this.atendidos ++;
	}
	
	//consigue el vuelo
	public String getVuelo() {
		return this.vuelo.getCodigo();
	}
	
	//consigue el codigo
	public int getCodigo() {
		return this.codigo;
	}
	
	//consigue el numero de atendidos en la puerta
	public int getAtendidos() {
		return this.atendidos;
	}
	
	//consigue estado
	public String getEstado() {
		return this.estado;
	}
	
	
}

