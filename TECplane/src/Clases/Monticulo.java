package Clases;

import java.util.ArrayList;

public class Monticulo<T> {
	
	public class elementoColaPrioridad<T> {
		
		T info;
		elementoColaPrioridad <T> left;
		elementoColaPrioridad <T> rigth;
		int prioridad;
		T info2;
		
		public elementoColaPrioridad (T info, elementoColaPrioridad <T> left,elementoColaPrioridad <T> rigth) {
			this.info= info;
			this.left = left;
			this.rigth  = rigth;
		}
		
		public elementoColaPrioridad(T info2, int prioridad) {
			this.info2 = info2;
			this.prioridad = prioridad;
		}
	
		
		public T getInfo2() {
			return info2;
		}
		
		public int getPrioridad () {
			return prioridad;
		}
		
		public T getInfo() {
			return info;
		}
		
		public elementoColaPrioridad <T> getleft(){
			return left;
		}
		
		public elementoColaPrioridad <T> getrigth(){
			return rigth;
		}
		
	}

	ArrayList monticulo;
	
	public Monticulo() {
		monticulo = new ArrayList<elementoColaPrioridad<T>> ();
	}
	
	public int getSize() {
		return monticulo.size();
	}
	
	public boolean isVacio () {
		return monticulo.isEmpty();
	}
	
	public T getElement(int index) {
		elementoColaPrioridad elemento = (elementoColaPrioridad) monticulo.get(index);
		return (T) elemento.getInfo2();
	}
	
	public int getPrioridad() {
		elementoColaPrioridad elemento = (elementoColaPrioridad) monticulo.get(0);
		return elemento.getPrioridad();
	}
	
	// arbol de minimos
	
	public <T> void insertar ( int prioridad, T info) {
		elementoColaPrioridad <T> prueba = new elementoColaPrioridad ( info, prioridad);
		
		int posPrueba= monticulo.size();
		int posPadre = (posPrueba- 1) /2 ;
		
		// agregar al final del arbol
		monticulo.add(prueba);
		
		// recolectar el nuevo elemento 
		while (posPrueba > 0 && prioridad > ((elementoColaPrioridad) monticulo.get(posPadre)).getPrioridad ()){
			
			//insertar los elementos
			monticulo.set(posPrueba, monticulo.get(posPadre));
			monticulo.set(posPadre, prueba);
			
			// redefinir los indices
			posPrueba = posPadre;
			posPadre = (posPrueba -1) / 2 ;
		}
		
	}
	
	public T extraer() {
		
		T info = null;
		boolean colocado = false;
		int posHueco, posHijo1,posHijo2, posHijoMin;
		
		if (!isVacio ()) {
			// Objeto de la raíz
			info= (T) ((elementoColaPrioridad) monticulo.get(0)).getInfo();
			
			//Eliminar elemento
			posHueco= getSize () -1;
			elementoColaPrioridad hueco = (elementoColaPrioridad) monticulo.remove(posHueco);
			
			//Reorganizar el arbol
			if (posHueco > 0) {
				posHueco = 0 ;
				monticulo.set(posHueco, hueco);
				
				//Comprobar el orden mientras tenga hijos
				
				posHijo1 = posHueco * 2 + 1;
				posHijo2 = posHueco * 2 + 1;
				while (!colocado && posHijo1 < getSize()) {
					posHijoMin = posHijo1;
					
					// Hallar el minimo de los hijos
					if ((posHijo2 < getSize ()) && ((elementoColaPrioridad) monticulo.get(posHijo2))
							.getPrioridad() < ((elementoColaPrioridad) monticulo.get(posHijo1)).getPrioridad()){
						posHijoMin = posHijo2;	
					}
					
					//Comparar el hijo menor con el nuevo padre
					if (hueco.getPrioridad() > ((elementoColaPrioridad) monticulo.get(posHijoMin)).getPrioridad()) {
						// hueco > hijo => intercambiar
						monticulo.set(posHueco,  monticulo.get(posHijoMin));
						monticulo.set(posHijoMin, hueco);
						
						//recalcular indices
						posHueco = posHijoMin;
						posHijo1 = posHueco * 2 +1;
						posHijo2 = posHueco * 2 +2;
					}
					else {
						 // posicion correcta
						colocado= true;
						
					}
				}
			}
		}
		return info;
	}
	
	// imprimir
	public void print () {
		for (int i = 0; i < monticulo.size(); i++) {
			System.out.println(((elementoColaPrioridad) monticulo.get(i)).getInfo2().getClass());
		}
	}
	public static void main(String[] args){
		Usuario us1 = new Usuario("us 1","14.12","123","cr","cr","pty",34312);

		Usuario us2 = new Usuario("usuario 2","14.12","123","cr","cr","pty",2132);
		
		Monticulo D = new Monticulo ();
		
		D.insertar(1, us1);
		D.insertar(2, us2);
		
		D.print();
		
		D.extraer();
		
		System.out.println("extrae");
		
		D.print();
		
		System.out.println("agrega");
		
		Usuario us3 = new Usuario("us 3","14.12","123","cr","cr","pty",31321);
		Usuario us4 = new Usuario("us 4","14.12","123","cr","cr","pty",32132);
		
		D.insertar(1, us4);
		D.insertar(2, us3);
		D.print();
		
		System.out.println("extrae");
		D.extraer();
		
		D.print();
		
	}
}

