package Clases;

public class Cola<T> {
	
	private Nodo<T> front;
	private Nodo<T> rear;
	private int size;
	
	// METODO CONSTRUCTOR
	public Cola(){
		this.front = new Nodo<>();
		this.rear = this.front;
		this.size = 0;
	}
	
	//AGREGAR NODOS A LA COLA
	public void insertar(T element){	
		this.rear.setNext(new Nodo<T>(element, null));
		this.rear = rear.getNext();
		this.size++;	
	}
	
	//ELIMINAR ELEMENTOS DE LA COLA
	public T extraer(){
		if(this.size == 0){
			System.out.println("No hay elementos en la cola");
			return null;
		}
		T temp = this.front.getNext().getElement();
		Nodo<T> nTemp = this.front.getNext();
		this.front.setNext(nTemp.getNext());
		if (this.rear == nTemp){
			this.rear = this.front;
		}
		this.size--;
		return temp;
	}
	
	public T getElement(Nodo nodo) {
		return (T) nodo.getElement();
	}
	
	public Nodo getFront() {
		return this.front;
	}
	
	public Nodo getRear() {
		return this.rear;
	}
	
	// OBTENER EL TAMAÑO DE LA COLA
	public int getSize(){
		return this.size;
	}
	
	// LIMPIAR TODA LA COLA
	public void clear(){
		this.front = new Nodo<>();
		this.rear = this.front;
		this.size = 0;
	}
	
	//OBTENER LOS DATOS DE LOS PASAJEROS
	public String toString(){
		String result = "";
		Nodo<T> tFront = this.front;
		int tSize = this.size;
		while(tSize != 0){
			result+= tFront.getNext().getElement().toString() + " ";
			tFront = tFront.getNext();
			tSize--;
		}
		return result;
	}


}