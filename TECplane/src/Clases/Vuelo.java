package Clases;

public class Vuelo {
	
	/*atributos de un vuelo, el atributo codigo es para diferenciar al avion. La letra corresponde a la posicion 
	* del asiento P si es pasillo, V de ventaco y C de centro. La cantidad de asientos esta distribuida de manera que 
	* hay 30 para los especiales, 50 para los economicoa y 20 para los VIP.*/
	private String lugarOrigen;
	private String lugarDestino;
	private String codigo;
	private int asiento = 0;
	private char letra = 'V';
	private Puerta puerta;
	
	//crea un vuelo
	public Vuelo(String lugarOrigen,String lugarDestino, String codigo) {
		this.lugarOrigen = lugarOrigen;
		this.lugarDestino = lugarDestino;
		this.codigo = codigo;
	}
	
	/*crea el asiento del usuario dependiendo del tipo de servicio y el numero y posicion que le corresponda,
	* a la vez que actualiza los datos, retorna asiento error si ya no hay mas asientos disponibles*/
	public String Asiento(Servicio servicio) {
		String asiento = null;
		if (this.asiento < 100) {
			asiento = '2' + Integer.toString(this.asiento) + letra;
			this.asiento ++;
			if (this.letra == 'V') {
				this.setLetra('C');
			}
			else if (this.letra == 'C') {
				this.setLetra('P');
			}
			else {
				this.setLetra('V');
			}
		}
		
		else {
			return "error";
		}
		return asiento;
	}
	
	//aumenta el asiento
	public void aumentarAsiento() {
		this.asiento ++;
	}
	
	//modifica la letra del servicio VIP
	public void setLetra(char letra) {
		this.letra = letra;
	}
	
	//modifica la puerta de especial
	public void setPuerta(String puesta) {
		this.puerta = puerta;
	}
	
	//consigue el asiento de VIP
	public int getAsiento() {
		return this.asiento;
	}
	
	//consigue el codigo del vuelo
	public String getCodigo() {
		return this.codigo;
	}
	
	//consigue la puerta de especial
	public Puerta getPuerta() {
		return this.puerta;
	}
	
	//consigue el numero de asiento
	public char getLetra() {
		return this.letra;
	}

	public String getOrigen() {
		return this.lugarOrigen;
	}

	public String getDestino() {
		return this.lugarDestino;
	}
	
	

}
