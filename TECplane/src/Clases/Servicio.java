package Clases;

public class Servicio{
	
	//informacion del servicio
	private int atendidos;
	private Cola colaC;
	private Monticulo colaM;
	private String metodo;
	private String tipo;
	private int tiempo = 0;
	
	//crea el servicio
	public Servicio(String tipo,String metodo) {
		this.tipo = tipo;
		this.metodo = metodo;
		if (metodo == "cola") {
			this.colaC = new Cola();
		} else {
			this.colaM = new Monticulo();
		}
	}
	
	//aumeta el numero de atendidos
	public void aumentarAtendidos() {
		this.atendidos ++;
	}
	
	//aumentar el tiempo
	public void sumarTiempo(int timepo) {
		this.tiempo += tiempo;
	}
	
	//consigue la cantidad de atendidos
	public int getAtendidos() {
		return this.atendidos;
	}
	
	//consigue el tipo de servicio
	public String getTipo() {
		return this.tipo;
	}
	
	//consigue el tipo de servicio
	public String getMetodo() {
		return this.metodo;
	}
	
	//consigue el tiempo
	public int getTiempo(){
		return this.tiempo;
	}
	//consigue la cola
	public Cola getCola() {
		return this.colaC;
	}
	public Monticulo getMonticulo() {
		return this.colaM;
	}
	
	//modifica la cola
	public void setCola(Cola cola) {
		this.colaC = cola;
	}
	public void setMonticulo(Monticulo cola) {
		this.colaM = cola;
	}
	
}
