package Clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.language.v1.Document;
import com.google.cloud.language.v1.Document.Type;
import com.google.cloud.language.v1.LanguageServiceClient;
import com.google.cloud.language.v1.Sentiment;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.common.collect.Lists;

public class Analizar {
	
	private String texto;

	public Analizar(String text) throws IOException{
//		File credentialsPath = new File("C:\\Users\\wonmi\\git\\tecplane\\TECplane\\TecPlane-1558cdda1ab4.json");
//		GoogleCredentials credentials = null;
//		try (FileInputStream serviceAccountStream = new FileInputStream(credentialsPath)) {
//		  credentials = ServiceAccountCredentials.fromStream(serviceAccountStream);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		Storage storage = StorageOptions.newBuilder()
//			    .setCredentials(credentials)
//			    .build()
//			    .getService();
		GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream("C:\\Users\\wonmi\\git\\tecplane\\TECplane\\TecPlane-1558cdda1ab4.json"))
		        .createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
		Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();

//		Storage storage1 = StorageOptions.newBuilder()
//			    .setCredentials(new GoogleCredentials(new AccessToken(accessToken, expirationTime)))
//			    .build()
//			    .getService();
		LanguageServiceClient language = LanguageServiceClient.create();
		
		// The text to analyze
		Document doc = Document.newBuilder()
		         .setContent(text).setType(Type.PLAIN_TEXT).build();
	
		// Detects the sentiment of the text
		Sentiment sentiment = language.analyzeSentiment(doc).getDocumentSentiment();
		
	    this.texto = "Analisis de sentimento:\n" + "Sentimiento: " + sentiment.getScore() + "\nMagnitud: " + sentiment.getMagnitude();
	    System.out.printf(texto);
		   
	}
	
	public String getTexto() {
		return this.texto;
	}
	
	public static void main(String[] args) throws IOException {
		Analizar A = new Analizar("Hola mundo");
		
	}
}
