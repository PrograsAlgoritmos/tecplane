package Clases;

public class Usuario {
	
	//informacion del usuario
	private String nombre;
	private String fechaNacimiento;
	private String pasaporte;
	private String nacionalidad;
	private String lugarOrigen;
	private String lugarDestino;
	private Servicio servicio;
	private String asiento;
	private String estado;
	private Vuelo vuelo;
	private int horaEntrada;
	
	//crea un usuario, el asiento se genera en otro metodo en la clase vuelo
	public Usuario (String nombre, String fechaNacimiento, String pasaporte, 
			String nacionalidad, String lugarOrigen,String lugarDestino, int horaEntrada) {
		this.nombre = nombre;
		this.fechaNacimiento = fechaNacimiento;
		this.pasaporte = pasaporte;
		this.nacionalidad = nacionalidad;
		this.lugarOrigen = lugarOrigen;
		this.lugarDestino = lugarDestino;
		this.horaEntrada = horaEntrada;
		this.estado = "espera";
	}
	
	//modifica el nombre
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	//modifica la fecha de nacimiento
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	//modifica el pasaporte
	public void setPasaporte(String pasaporte) {
		this.pasaporte = pasaporte;
	}
	
	//modifica la nacionalidad
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	//modifica el lugar de origen
	public void setLugarOrigen(String lugarOrigen) {
		this.lugarOrigen = lugarOrigen;
	}
	
	//modifica el lugar de destino
	public void setLugarDestino(String lugarDestino) {
		this.lugarDestino = lugarDestino;
	}

	//modifica el tipo de servicio
	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}
	
	//modifica el asiento
	public void setAsiento(String asiento) {
		this.asiento = asiento;
	}
	
	//modifica el vuelo
	public void setVuelo(Vuelo vuelo) {
		this.vuelo = vuelo;
	}
		
	//consigue el nombre
	public String getNombre() {
		return this.nombre;
	}

	//consigue la fecha de nacimiento
	public String getFechaNacimiento() {
		return this.fechaNacimiento;
	}
	
	//consigue el pasaporte
	public String getPasaporte() {
		return this.pasaporte;
	}
	
	//consigue la nacionalidad
	public String getNacionalidad() {
		return this.nacionalidad;
	}
	
	//consigue el lugar de origen
	public String getLugarOrigen() {
		return this.lugarOrigen;
	}
	
	//consigue el lugar de destino
	public String getLugarDestino() {
		return this.lugarDestino;
	}
	
	//consigue el tipo de servicio
	public Servicio getServicio() {
		return this.servicio;
	}
	
	//consigue el asiento
	public String getAsiento() {
		return this.asiento;
	}
	
	//consigue el vuelo
	public Vuelo getVuelo(){
		return this.vuelo;
	}
	
	//consigue la hora de slaida
	public int getHoraEntrada(){
		return this.horaEntrada;
	}
	
	//Consigue todos los datos
	public String toString() {
		return "Nombre: " + this.nombre + "Fecha de nacimiento: " + this.fechaNacimiento + "\nNacionalidad: " + this.nacionalidad + "\nPasaporte: " + 
				this.pasaporte + "\nLugar de origen: " + this.lugarOrigen + "\n Lugar de destino: " + this.lugarDestino;
	}

}
