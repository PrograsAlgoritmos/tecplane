package Clases;

public class Lista<T> {
    
	private Nodo<T> head;
	private Nodo<T> current;
	private Nodo <T> tail;
	private int position;
	private int size;
	
	//constructores LinkedList
	
	/**
	 * Contructor predeterminado
	 */
	public Lista() {
		this.head = new Nodo<T>();
		this.current = this.head;
		this.tail = this.head;
		this.size = 0;
		this.position = -1;
	}

	public void insertar(T element) {
		//insertar en cualquier posiciÃ³n
		Nodo newNode = new Nodo(element, this.current.getNext());
		this.current.setNext(newNode);
		//necesario si se está insertando al final de la lista
		if (this.current == this.tail) {
			this.tail = tail.getNext();
		}
		this.size++;
		
	}
	
	public void append(T element) {
		//siempre se pega al final de la lista
		Nodo newNode = new Nodo(element);
		this.tail.setNext(newNode);
		this.tail = newNode;
		this.size++;
	}
	
	public void eliminar() {
		//verificar que la lista no está vacía
		if ((this.head == this.current) && (this.head == this.tail)){
			System.out.println("Lista vacia, no se puede remover ningun elemento");
			return;
		} //también if (this.size == 0) ...
		
		//en temp se va a almacenar el nodo ANTERIOR al que se quiere borrar
		Nodo temp = head;
		while (temp.getNext() != this.current) {
			temp = temp.getNext();
		}
		//borrar la referencia al nodo actual
		temp.setNext(this.current.getNext());
		//necesario si se esta borrando el último nodo
		if (this.current == this.tail) {
			this.current = this.tail = temp;
			this.position--;
		}
		else
			//necesario si se está borrando un nodo diferente al último
			this.current = this.current.getNext();
		
		//disminuir el tamaño
		this.size--;
	}
	
	public void Limpiar() {
		this.head = this.tail = this.current = new Nodo();
		this.position = -1;
		this.size = 0;
	}
	
	public T getElement(){
		return this.current.getElement();
	}	
	
	public int getSize() {
		return this.size;
	}
	
	public boolean next() {
		if (this.current == this.tail) {
			System.out.println("Actualmente en último nodo, no se puede avanzar");
			return false;
		}
		this.current = this.current.getNext();
		this.position++;
		return true;
	}
	
	public boolean previous() {
		if (this.current == this.head) {
			System.out.println("Actualmente en primer nodo, no se puede retroceder");
			return false;
		}
		Nodo temp = head;
		this.position = -1;
		while (temp.getNext() != this.current) {
			temp = temp.getNext();
			this.position++;
		}
		this.current = temp;
		return true;
	}
	
	public int getPosition() {
		return this.position;	
	}
	
	public void goToStart(){
		this.current = this.head;
		this.position = -1;
	}
	
	public void goToEnd(){
		this.current = this.tail;
		this.position = this.size - 1;
	}
	
	public void goToPos(int pos) {
		if (pos < 0 || pos >= this.size) {
			System.out.println("Posición inválida");
			return;
		}
		if (pos > this.position) {
			while (pos > this.position) {
				this.next();        
			}
		} else if (pos < this.position) {
			while (pos < this.position) {
				this.previous();
			}
		}
	}
	
	public int getPositionOfElement(T element) {
		Nodo tempNode = this.head;
		int position = -1;
		while (tempNode != null) {
			if (tempNode.getElement() != null && tempNode.getElement().equals(element)){
				return position;
			}
			tempNode = tempNode.getNext();
			position++;
		}
		return -1;
	}
        
	public String toString() {
		Nodo currentNode = this.head.getNext();
		
		StringBuffer result = new StringBuffer();
        
        for (int i = 0; currentNode != null; i++) 
		{
        	if (i > 0) 
			{
        		result.append(",");
        	}
        	T element = (T) currentNode.getElement();
        	result.append(element == null ? "" : element);
        	currentNode = currentNode.getNext();
        }
        return result.toString();
	
	}
}