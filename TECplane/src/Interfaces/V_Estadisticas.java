package Interfaces;

import java.awt.EventQueue;
import java.awt.Dialog.ModalExclusionType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import Clases.Lista;
import Clases.Puerta;
import Clases.Servicio;

import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTextArea;
import java.awt.SystemColor;
import javax.swing.JButton;

public class V_Estadisticas extends JFrame{
	private JTable tblServicios;
	private Servicio Especial = V_config_inicial.servicioEsp;
	private Servicio Platino = V_config_inicial.servicioPla;
	private Servicio Oro = V_config_inicial.servicioOro;
	private Servicio Economico = V_config_inicial.servicioEco;
	private Lista listaP = V_config_inicial.lista_p;
	private JTable tblPuertas;
	
	public V_Estadisticas() {
		getContentPane().setBackground(new Color(0, 191, 255));
		setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(true);
		setTitle("Estadisticas");
		setBounds(100, 100, 431, 313);
		getContentPane().setLayout(null);
		
		
		Object[][] datosServ = {{"Especiales",Especial.getTiempo()/Especial.getAtendidos(),Especial.getAtendidos()},
				{"Platino",Platino.getTiempo()/Platino.getAtendidos(),Platino.getAtendidos()},
				{"Oro",Oro.getTiempo()/Oro.getAtendidos(),Oro.getAtendidos()},
				{"Economico",Economico.getTiempo()/Economico.getAtendidos(),Economico.getAtendidos()},
				{"Salida"}};
		String[] titulosServ = {"Servidores","Tiempo Espera Promedio","Total Atendidos"};
		DefaultTableModel dtmServ = new DefaultTableModel(datosServ,titulosServ);
		dtmServ.setColumnCount(3);
		dtmServ.setRowCount(6);
		
		Object[][] datosPuer = null;
		String[] titulosPuer = {"Puerta","Personas Atendidas"};
		DefaultTableModel dtmPuer = new DefaultTableModel(datosPuer,titulosPuer);
		dtmPuer.setColumnCount(3);
		int indice = 0;
		while (indice < listaP.getSize()) {
			listaP.goToPos(indice);
			Puerta puerta = (Puerta) listaP.getElement();
			Object[] dato = {puerta.getCodigo(),puerta.getAtendidos()};
			dtmPuer.addRow(dato);
			indice++;
		}
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 11, 397, 105);
		
		getContentPane().add(scrollPane);
		tblServicios = new JTable(dtmServ);
		scrollPane.setViewportView(tblServicios);
		tblServicios.setRowSelectionAllowed(false);
		tblServicios.setEnabled(false);
		tblServicios.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JButton btnCerrar = new JButton("Cerrar");
		btnCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_Estadisticas.main(null);
				setVisible(false);
			}
		});
		btnCerrar.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnCerrar.setBounds(318, 240, 89, 23);
		getContentPane().add(btnCerrar);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 127, 296, 136);
		getContentPane().add(scrollPane_1);
		
		tblPuertas = new JTable(dtmPuer);
		tblPuertas.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tblPuertas.setEnabled(false);
		tblPuertas.setRowSelectionAllowed(false);
		scrollPane_1.setViewportView(tblPuertas);
		
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_Estadisticas frame = new V_Estadisticas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
