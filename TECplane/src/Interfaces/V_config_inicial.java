package Interfaces;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;

import Clases.Lista;
import Clases.Puerta;
import Clases.Servicio;
import Clases.Vuelo;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ButtonGroup;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class V_config_inicial extends JFrame {
	static V_config_inicial frame = new V_config_inicial();
	public final ButtonGroup Eleccion_Serv = new ButtonGroup();
	private final JSpinner spinner_cantidad_puertas = new JSpinner();
	private final JSpinner spinner_tiempo_min = new JSpinner();
	private final JSpinner spinner_tiempo_max = new JSpinner();
	public static int cantPuertas;
	public static int tiempoMin;
	public static int tiempoMax;
	public static String metodoCola;
	public static Lista<Vuelo> lista_v;
	public static Lista<Puerta> lista_p;
	public static Servicio servicioEsp;
	public static Servicio servicioPla;
	public static Servicio servicioEco;
	public static Servicio servicioOro;
	public static Servicio servicioSal;
	 /*
	  * Se inicializa la interfaz
	  */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/*
	 * Se crean los objetos que va utilizar la interfaz
	 */
	public V_config_inicial() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 512, 200);
		Panel_De_Imagenes fondo = new Panel_De_Imagenes("Imagenes/fondo_configuracion.png");
		fondo.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(fondo);
		
		/*
		 * Label de servicio especial, indica el nombre del servicio
		 */
		fondo.setLayout(null);
		
		/*
		 * Eleccion que tiene el usuario del metodo a implementar
		 */
		JRadioButton rdbtn_Mtd_Cola_Serv = new JRadioButton("Metodo Cola");
		Eleccion_Serv.add(rdbtn_Mtd_Cola_Serv);
		rdbtn_Mtd_Cola_Serv.setFont(new Font("Tahoma", Font.PLAIN, 11));
		rdbtn_Mtd_Cola_Serv.setBounds(0, 41, 109, 23);
		rdbtn_Mtd_Cola_Serv.setOpaque(false);
		rdbtn_Mtd_Cola_Serv.setActionCommand("Método Cola");
		fondo.add(rdbtn_Mtd_Cola_Serv);
		
		/*
		 * Eleccion que tiene el usuario del metodo a implementar
		 */
		JRadioButton rdbtn_Mtd_Arbol_Serv = new JRadioButton("Metodo Arbol");
		Eleccion_Serv.add(rdbtn_Mtd_Arbol_Serv);
		rdbtn_Mtd_Arbol_Serv.setFont(new Font("Tahoma", Font.PLAIN, 11));
		rdbtn_Mtd_Arbol_Serv.setBounds(0, 67, 109, 23);
		rdbtn_Mtd_Arbol_Serv.setOpaque(false);
		rdbtn_Mtd_Arbol_Serv.setActionCommand("Método Árbol");
		fondo.add(rdbtn_Mtd_Arbol_Serv);
	
		/*
		 * Label de cantidad de puertas, indica el nombre par guia del usuario
		 */
		JLabel lblCantidadDePuertas = new JLabel("Cantidad de puertas");
		lblCantidadDePuertas.setBounds(161, 45, 109, 14);
		lblCantidadDePuertas.setFont(new Font("Tahoma", Font.PLAIN, 11));
		fondo.add(lblCantidadDePuertas);
		
		/*
		 * Creacion del boton configurar
		 */
		JButton btnConfigurar = new JButton("Configurar");
		btnConfigurar.setBounds(375, 119, 121, 42);
		btnConfigurar.setContentAreaFilled(false);
		btnConfigurar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				configurar();
			}
		});
		fondo.add(btnConfigurar);
		
		/*
		 * En este label se asigna el titulo de "Tiempo de salida" en la interfaz
		 */
		JLabel lblSalida = new JLabel("Tiempo salida");
		lblSalida.setForeground(Color.BLUE);
		lblSalida.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblSalida.setBounds(122, 87, 148, 27);
		fondo.add(lblSalida);
		
		/*
		 * En este label se asigan el titulo de "tiempo min" para que el usuario ingrese los datos
		 */
		JLabel lbl_tiempo_min = new JLabel("Tiempo min");
		lbl_tiempo_min.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lbl_tiempo_min.setBounds(10, 133, 62, 14);
		fondo.add(lbl_tiempo_min);
		
		/*
		 * En este label se asigan el titulo de "tiempo max" para que el usuario ingrese los datos
		 */
		JLabel lbl_tiempo_max = new JLabel("Tiempo max");
		lbl_tiempo_max.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lbl_tiempo_max.setBounds(208, 133, 62, 14);
		fondo.add(lbl_tiempo_max);
		
		/*
		 * spinner para ingresar la cantidad de puertas del servicio especial
		 */
		spinner_cantidad_puertas.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinner_cantidad_puertas.setBounds(286, 42, 49, 20);
		fondo.add(spinner_cantidad_puertas);
		
		/*
		 * spinner para ingresar la cantidad de tiempo min
		 */
		spinner_tiempo_min.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinner_tiempo_min.setBounds(99, 130, 49, 20);
		fondo.add(spinner_tiempo_min);
		
		/*
		 * spinner para ingresar la cantidad de tiempo max
		 */
		spinner_tiempo_max.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinner_tiempo_max.setBounds(286, 130, 49, 20);
		fondo.add(spinner_tiempo_max);
		
		//Escoge el valor de la opcion escogido en el radio button
		if(rdbtn_Mtd_Arbol_Serv.isSelected()) {
			metodoCola = "monticulo";
		} else {
			metodoCola = "cola";
		}
		servicioEsp = new Servicio("especial",metodoCola);
		servicioOro = new Servicio("platino",metodoCola);
		servicioPla = new Servicio("oro",metodoCola);
		servicioEco = new Servicio("economico",metodoCola);
		servicioSal = new Servicio("salida",metodoCola);
		
		JLabel lbl_servicios = new JLabel("Configuraci\u00F3n");
		lbl_servicios.setForeground(Color.BLUE);
		lbl_servicios.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lbl_servicios.setBounds(122, 0, 148, 27);
		fondo.add(lbl_servicios);
		
		Vuelo v1 =new Vuelo("Costa Rica", "Colombia", "12D");
		Vuelo v2= new Vuelo("Costa Rica", "U.S.A", "90F");
		Vuelo v3= new Vuelo("Costa Rica", "España", "301F");
		Vuelo v4= new Vuelo("Costa Rica", "Noruega", "45E");
		Vuelo v5= new Vuelo("Costa Rica", "Pánama", "33T");
		Vuelo v6= new Vuelo("Costa Rica", "Bélgica", "15V");

		lista_v = new Lista<Vuelo>();
		lista_v.append(v1);
		lista_v.append(v2);
		lista_v.append(v3);
		lista_v.append(v4);
		lista_v.append(v5);
		lista_v.append(v6);	
		
	}
	
	public void configurar() {
		/*
		 * En caso de estar la informacion completa, se puede continuar con el programa
		 */
		
		cantPuertas =  (int) spinner_cantidad_puertas.getValue();
		tiempoMin =  (int) spinner_tiempo_min.getValue();
		tiempoMax =  (int) spinner_tiempo_max.getValue();
		
		//En caso de que la informacion se encuentre completa puede entrar en el if
		if(	Eleccion_Serv.getSelection() != null && cantPuertas != 0 && tiempoMax != 0){
			
//			//En caso de haber escogido el metodo cola
//			if(metodoCola.equals("cola")) {	
//				metodo_cola = new Cola<Vuelo>();
//				//instancia todas las colas de los vuelos, segun la cantidad de vuelos almacenados en la lista.
//				for(int i=1; i<listaVuelos.getSize(); i++) {
//					listaVuelos.goToPos(i);
//					metodo_cola.insertar(listaVuelos.getElement());	
//				}
//			}
//			
//			//En caso de haber escogido el metodo heap
//			if(valor_eleccion_metodo.equals("arbol")) {
//				
//			}
			
			//instancia todas las puertas automaticamente segun la cantidad de puertas asignada
			lista_p = new Lista<Puerta>();
			for(int u=1; u<cantPuertas; u++) {
				Puerta puerta = new Puerta(u);
				lista_p.append(puerta);
			}
			
			JOptionPane.showMessageDialog(null, "Podemos continuar","Continuar",JOptionPane.PLAIN_MESSAGE);
			frame.setVisible(false);
			V_vuelos ventana_V = new V_vuelos();
			ventana_V.setVisible(true);
		}
		
		/*
		 * En caso de no estar completa la informacion, muestra un dialogo indicando que debe indicar la informacion completa
		 */
		else {
			JOptionPane.showMessageDialog(null, "Debe indicar todas las opciones antes de continuar","Error",JOptionPane.ERROR_MESSAGE);
		}
	}
}