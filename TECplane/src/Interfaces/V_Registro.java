package Interfaces;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.awt.Dialog.ModalExclusionType;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import Clases.Lista;
import Clases.Servicio;
import Clases.Usuario;
import Clases.Vuelo;

import javax.swing.JButton;
import java.awt.Color;

public class V_Registro extends JFrame{
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField txtNombre;
	private JTextField txtFecha;
	private JTextField txtNacionalidad;
	private JTextField txtPasaporte;
	private JTextField txtOrigen;
	private JTextField txtDestino;
	private Servicio Especial = V_config_inicial.servicioEsp;
	private Servicio Platino = V_config_inicial.servicioPla;
	private Servicio Oro = V_config_inicial.servicioOro;
	private Servicio Economico = V_config_inicial.servicioEco;
	private Lista listaVuelos = V_config_inicial.lista_v;
	private static int contadorEsp = -1;
	private static int contadorPla = -1;
	private static int contadorOro = -1;
	private static int contadorEco = -1;
	private DefaultTableModel dtmEsp = Ventana1.dtmEsp;
	private DefaultTableModel dtmPla = Ventana1.dtmPla;
	private DefaultTableModel dtmOro = Ventana1.dtmOro;
	private DefaultTableModel dtmEco = Ventana1.dtmEco;
	
	public V_Registro() {
		getContentPane().setBackground(new Color(0, 250, 154));
		getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 14));
		setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(true);
		setTitle("Registro");
		setBounds(100, 100, 449, 301);
		getContentPane().setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNombre.setBounds(10, 11, 66, 17);
		getContentPane().add(lblNombre);
		
		JLabel lblFechaDeNacimiento = new JLabel("Fecha de nacimiento");
		lblFechaDeNacimiento.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFechaDeNacimiento.setBounds(10, 39, 125, 17);
		getContentPane().add(lblFechaDeNacimiento);
		
		JLabel lblNacionalidad = new JLabel("Nacionalidad");
		lblNacionalidad.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNacionalidad.setBounds(10, 67, 83, 17);
		getContentPane().add(lblNacionalidad);
		
		JLabel lblPasaporte = new JLabel("Pasaporte");
		lblPasaporte.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPasaporte.setBounds(10, 95, 66, 17);
		getContentPane().add(lblPasaporte);
		
		JLabel lblOrigen = new JLabel("Lugar de origen");
		lblOrigen.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblOrigen.setBounds(10, 123, 103, 17);
		getContentPane().add(lblOrigen);
		
		JLabel lblDestino = new JLabel("Lugar de destino");
		lblDestino.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDestino.setBounds(10, 151, 103, 17);
		getContentPane().add(lblDestino);
		
		JLabel lblServicio = new JLabel("Servicio");
		lblServicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblServicio.setBounds(10, 179, 66, 17);
		getContentPane().add(lblServicio);
		
		JRadioButton rdbtnEspecial = new JRadioButton("Especial");
		rdbtnEspecial.setBackground(new Color(0, 250, 154));
		buttonGroup.add(rdbtnEspecial);
		rdbtnEspecial.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnEspecial.setBounds(20, 203, 109, 23);
		getContentPane().add(rdbtnEspecial);
		
		JRadioButton rdbtnPlatino = new JRadioButton("Platino");
		rdbtnPlatino.setBackground(new Color(0, 250, 154));
		buttonGroup.add(rdbtnPlatino);
		rdbtnPlatino.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnPlatino.setBounds(131, 203, 109, 23);
		getContentPane().add(rdbtnPlatino);
		
		JRadioButton rdbtnOro = new JRadioButton("Oro");
		rdbtnOro.setBackground(new Color(0, 250, 154));
		buttonGroup.add(rdbtnOro);
		rdbtnOro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnOro.setBounds(131, 229, 109, 23);
		getContentPane().add(rdbtnOro);
		
		JRadioButton rdbtnEconomico = new JRadioButton("Economico");
		rdbtnEconomico.setBackground(new Color(0, 250, 154));
		buttonGroup.add(rdbtnEconomico);
		rdbtnEconomico.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnEconomico.setBounds(20, 229, 109, 23);
		getContentPane().add(rdbtnEconomico);
		
		txtNombre = new JTextField();
		txtNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtNombre.setBounds(160, 11, 252, 20);
		getContentPane().add(txtNombre);
		txtNombre.setColumns(10);
		
		txtFecha = new JTextField();
		txtFecha.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtFecha.setColumns(10);
		txtFecha.setBounds(160, 39, 252, 20);
		getContentPane().add(txtFecha);
		
		txtNacionalidad = new JTextField();
		txtNacionalidad.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtNacionalidad.setColumns(10);
		txtNacionalidad.setBounds(160, 67, 252, 20);
		getContentPane().add(txtNacionalidad);
		
		txtPasaporte = new JTextField();
		txtPasaporte.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtPasaporte.setColumns(10);
		txtPasaporte.setBounds(160, 95, 252, 20);
		getContentPane().add(txtPasaporte);
		
		txtOrigen = new JTextField();
		txtOrigen.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtOrigen.setColumns(10);
		txtOrigen.setBounds(160, 123, 252, 20);
		getContentPane().add(txtOrigen);
		
		txtDestino = new JTextField();
		txtDestino.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtDestino.setColumns(10);
		txtDestino.setBounds(160, 151, 252, 20);
		getContentPane().add(txtDestino);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnAceptar.setBounds(323, 231, 89, 23);
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (txtNombre.getText()!="" && txtFecha.getText()!="" && txtPasaporte.getText()!="" && 
						txtNacionalidad.getText()!="" && txtOrigen.getText()!="" && txtDestino.getText()!="") {
					if (rdbtnEspecial.isSelected() || rdbtnPlatino.isSelected() || rdbtnOro.isSelected() || rdbtnEconomico.isSelected()) {
						Calendar calendario = Calendar.getInstance();
						int segundos = calendario.get(Calendar.MINUTE) * 60 + calendario.get(Calendar.SECOND);
						int contador = 0;
						String asiento = null;
						Usuario usuario = new Usuario(txtNombre.getText(),txtFecha.getText(),txtPasaporte.getText(),txtNacionalidad.getText(),
								txtOrigen.getText(),txtDestino.getText(),segundos);
						if(rdbtnEspecial.isSelected()) {
							usuario.setServicio(Especial);
							contador = contadorEsp ++;
							asiento = "E";
						} else if(rdbtnPlatino.isSelected()) {
							usuario.setServicio(Platino);
							contador = contadorPla++;
							asiento = "P";
						} else if(rdbtnOro.isSelected()) {
							usuario.setServicio(Oro);
							contador = contadorOro++;
							asiento = "O";
						} else if(rdbtnEconomico.isSelected()) {
							usuario.setServicio(Economico);
							contador = contadorEco++;
							asiento = "N";
						}
						if (usuario.getServicio().getMetodo()=="cola") {
							usuario.getServicio().getCola().insertar(usuario);
						} else {
							usuario.getServicio().getMonticulo().insertar(contador,usuario);
						}
						listaVuelos.goToStart();
						while (listaVuelos.getPosition() < listaVuelos.getSize()) {
							Vuelo vuelo = (Vuelo) listaVuelos.getElement();
							if (vuelo.getOrigen() == txtOrigen.getText() && vuelo.getDestino() == txtDestino.getText()) {
								usuario.setVuelo(vuelo);
								asiento = Asiento(vuelo,asiento);
								usuario.setAsiento(asiento);
							}
						}
						imprimir (usuario);
						JOptionPane.showMessageDialog(null,"Numero de asiento asignado: " + asiento,"Asiento",JOptionPane.PLAIN_MESSAGE);
						V_Registro.main(null);
						setVisible(false);
					}
				} else {
					JOptionPane.showMessageDialog(null,"No ha llenado toda la informacion.","Error",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		getContentPane().add(btnAceptar);
		
	}
	
	public void imprimir (Usuario usuario) {
		Object [] dato = {usuario.getNombre(),usuario.getVuelo().getPuerta().getCodigo(),usuario.getVuelo().getCodigo()};
		if (usuario.getServicio().getTipo() == "especial") {
			dtmEsp.addRow(dato);
		} else if (usuario.getServicio().getTipo() == "platino") {
			dtmPla.addRow(dato);
		} else if (usuario.getServicio().getTipo() == "oro") {
			dtmOro.addRow(dato);
		} else {
			dtmEco.addRow(dato);
		}
		
	}
	
	/*crea el asiento del usuario dependiendo del tipo de servicio y el numero y posicion que le corresponda,
	* a la vez que actualiza los datos, retorna asiento error si ya no hay mas asientos disponibles*/
	public String Asiento(Vuelo vuelo, String servicio) {
		String asiento = null;
		int num = vuelo.getAsiento();
		asiento += Integer.toString(num) + vuelo.getLetra();
		vuelo.aumentarAsiento();;
		if (vuelo.getLetra() == 'V') {
			vuelo.setLetra('C');
		} else if (vuelo.getLetra() == 'C') {
			vuelo.setLetra('P');
		} else {
			vuelo.setLetra('V');
		} return asiento;
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_Registro frame = new V_Registro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
