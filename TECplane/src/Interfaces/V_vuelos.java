package Interfaces;

import Clases.Vuelo;
import Clases.Lista;
import Clases.Cola;
import Clases.Puerta;
import Clases.Usuario;
import Clases.Servicio;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import java.awt.Font;

import javax.swing.BoundedRangeModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;

public class V_vuelos extends JFrame {

	private JPanel contentPane;
	private JTextField textField_origen;
	private JTextField textField_destino;
	private JTextField textField_codigo;
	JList<String> Jlista_vuelos = new JList<String>();
	DefaultListModel<String> mostrar_vuelos = new DefaultListModel<String>();
	JComboBox<Integer> comboBox_puerta = new JComboBox<Integer>();
	private Lista listaVuelos = V_config_inicial.lista_v;
	private Lista listaPuertas = V_config_inicial.lista_p;
	static V_vuelos frame = new V_vuelos();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_vuelos() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 207);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lbl_origen = new JLabel("Origen");
		lbl_origen.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lbl_origen.setBounds(10, 38, 48, 19);
		contentPane.add(lbl_origen);
		
		textField_origen = new JTextField();
		textField_origen.setBounds(104, 37, 96, 20);
		contentPane.add(textField_origen);
		textField_origen.setColumns(10);
		
		JLabel lbl_destino = new JLabel("Destino");
		lbl_destino.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lbl_destino.setBounds(10, 68, 48, 14);
		contentPane.add(lbl_destino);
		
		textField_destino = new JTextField();
		textField_destino.setBounds(104, 68, 96, 20);
		contentPane.add(textField_destino);
		textField_destino.setColumns(10);
		
		JLabel lbl_codigo = new JLabel("C\u00F3digo");
		lbl_codigo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lbl_codigo.setBounds(10, 93, 56, 20);
		contentPane.add(lbl_codigo);
		
		textField_codigo = new JTextField();
		textField_codigo.setBounds(104, 99, 96, 20);
		contentPane.add(textField_codigo);
		textField_codigo.setColumns(10);
		
		JLabel lblVuelos = new JLabel("Vuelos");
		lblVuelos.setForeground(Color.BLUE);
		lblVuelos.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblVuelos.setBounds(74, 0, 56, 26);
		contentPane.add(lblVuelos);
		
		JLabel lbl_puerta = new JLabel("Puerta");
		lbl_puerta.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lbl_puerta.setBounds(448, 38, 48, 19);
		contentPane.add(lbl_puerta);
		
		comboBox_puerta.setBounds(526, 38, 48, 22);
		contentPane.add(comboBox_puerta);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(229, 38, 209, 120);
		contentPane.add(scrollPane);
		scrollPane.setViewportView(Jlista_vuelos);
		
		Jlista_vuelos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		JButton btnCrear = new JButton("Crear");
		btnCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				agregar();
			}
		});
		btnCrear.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnCrear.setBounds(63, 130, 89, 23);
		contentPane.add(btnCrear);
		
		JButton btnAsignar = new JButton("Asignar");
		btnAsignar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				asignar();
			}
		});
		btnAsignar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnAsignar.setBounds(469, 84, 89, 23);
		contentPane.add(btnAsignar);
		
		JButton btnCerrar = new JButton("Cerrar");
		btnCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				agregar();
			}
		});
		btnCerrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnCerrar.setBounds(469, 130, 89, 23);
		contentPane.add(btnCerrar);
		
		for(int i=0;i<V_config_inicial.cantPuertas;i++) {
			comboBox_puerta.addItem(1+i);
		}
		
		/*
		 * Recorre la lista de vuelos creada en la clase V_config_inicial para agregarlos al JList
		 */
		for (int i=0; i<listaVuelos.getSize();i++) {
			listaVuelos.goToPos(i);
			Vuelo vuelo = (Vuelo) listaVuelos.getElement();
			mostrar_vuelos.addElement("Origen: " + vuelo.getOrigen() + "- Destino: "+ vuelo.getDestino() +"- Codigo: "+ vuelo.getCodigo());
			Jlista_vuelos.setModel(mostrar_vuelos);
		}
	}
	
	//Agregar vuelos de manera manual
	public void agregar() {
		
		String origen= textField_origen.getText();
		String destino= textField_destino.getText();
		String codigo = textField_codigo.getText();
		
		if(origen.equals("")&&destino.equals("")&&codigo.equals("")) {
			JOptionPane.showMessageDialog(null, "Debe indicar todas las opciones antes de continuar");
		}
		else {
			/*
			 * Agrega los datos recolectados de los text field para agregar vuelos automaticamente
			 */
			mostrar_vuelos.addElement("Origen: " + origen + "- Destino: "+ destino+"- Codigo: "+codigo);
			Jlista_vuelos.setModel(mostrar_vuelos);
			}
	}
	
	//Asignar la puerta al vuelo seleccionado	
	public void asignar(){
		
		//Revisa si el Jlist contiene informacion, en caso de no contener envia un mensaje de Joption Pane
		if(Jlista_vuelos.isSelectionEmpty()||comboBox_puerta.getSelectedItem()==null) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un vuelo y una puerta, sino debe crearlo");
		}
		//En caso de contener infomacion entra en el else
		else {
			/*
			 * Asignacion de vuelos a las puertas
			 */
			int numero_puerta = (int) comboBox_puerta.getSelectedItem(); //Toma el valor obtenido(numero de puerta) del comboBox
			int cantidad_puertas = listaPuertas.getSize();  // Toma el valor de la cantidad de puertas creadas en la clase V_config_inicial
			
			//En este for se fija la cantidad de puertas creadas en la clase V_config_incial despues empieza a comparar el dato seleccionadp en el comboBox 
			for (int a=0; a<cantidad_puertas; a++) {
				listaPuertas.goToPos(a); //Empieza a recorrer la lista desde la primera posicion
				Puerta puerta = (Puerta) listaPuertas.getElement();
				//Comparara el #Puerta seleccionado y se fija si existe en la lista creada en la clase V_config_inicial
				if(numero_puerta == puerta.getCodigo()) {
					
					//Empieza a recorrer a lista de vuelos y verificar si el vuelo seleccionado se encuentra en la lista
					for(int i=0;i<listaVuelos.getSize();i++) {
						listaVuelos.goToPos(i);
						Vuelo vuelo = (Vuelo) listaVuelos.getElement();
						//Revisa que el vuelo seleccionado se encuentra en la lista, si es verdadero, entonces asigna a la puerta seleccionado el vuelo seleccionado
						if(Jlista_vuelos.getSelectedValue().equals("Origen: " + vuelo.getOrigen() + "- Destino: "+ vuelo.getDestino() +"- Codigo: "+ vuelo.getCodigo())) {
							//SE ASIGNA A LA PUERTA SECCIONADA EL VUELO SELECCIONADO
							puerta.setVuelo(vuelo);
							//Se remueve el vuelo asignado
							mostrar_vuelos.removeElement(Jlista_vuelos.getSelectedValue());
							JOptionPane.showMessageDialog(null, "Se asigno correctamente");
							break;
						}
					}
				}
			}
			
			frame.setVisible(false);
			Ventana1 v1 = new Ventana1();
			v1.setVisible(true);
			
		}
	}	
}