package Interfaces;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTable;
import javax.swing.UIManager;
import java.awt.SystemColor;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import Clases.Lista;
import Clases.Nodo;
import Clases.Puerta;
import Clases.Servicio;
import Clases.Usuario;
import Clases.Vuelo;

public class Ventana1 extends JFrame {

	private JPanel contentPane;
	private JTable tblOro;
	private JTable tblPlatino;
	private JTable tblEspeciales;
	private JTable tblEconomico;
	private JTable tblVIP;
	private JTable tblSalida;
	private Servicio Especial = V_config_inicial.servicioEsp;
	private Servicio Platino = V_config_inicial.servicioPla;
	private Servicio Oro = V_config_inicial.servicioOro;
	private Servicio Economico = V_config_inicial.servicioEco;
	private Servicio Salida = V_config_inicial.servicioSal;
	private Lista<Puerta> listaP = V_config_inicial.lista_p;
	private String metodo = V_config_inicial.metodoCola;
	private DefaultTableModel dtmSal;
	public static DefaultTableModel dtmEsp;
	public static DefaultTableModel dtmPla;
	public static DefaultTableModel dtmOro;
	public static DefaultTableModel dtmEco;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana1 frame = new Ventana1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 943, 711);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnRegistro = new JButton("");
		btnRegistro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_Registro ventana = new V_Registro();
				ventana.setVisible(true);
			}
		});
		btnRegistro.setIcon(new ImageIcon(Ventana1.class.getResource("/Imagenes/registro.png")));
		btnRegistro.setBounds(10, 11, 90, 87);
		contentPane.add(btnRegistro);
		
		JButton btnConfigurar = new JButton("");
		btnConfigurar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_Registro ventana = new V_Registro();
				ventana.setVisible(true);
			}
		});
		btnConfigurar.setIcon(new ImageIcon(Ventana1.class.getResource("/Imagenes/configuracion.png")));
		btnConfigurar.setBounds(10, 109, 90, 93);
		contentPane.add(btnConfigurar);
		
		JButton btnVisualizar = new JButton("");
		btnVisualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_Visualizar ventana = new V_Visualizar();
				ventana.setVisible(true);
			}
		});
		btnVisualizar.setIcon(new ImageIcon(Ventana1.class.getResource("/Imagenes/visualizar.jpg")));
		btnVisualizar.setBounds(10, 213, 90, 87);
		contentPane.add(btnVisualizar);
		
		JButton btnEstadisticas = new JButton("");
		btnEstadisticas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_Estadisticas ventana = new V_Estadisticas();
				ventana.setVisible(true);
			}
		});
		btnEstadisticas.setIcon(new ImageIcon(Ventana1.class.getResource("/Imagenes/estadistica.png")));
		btnEstadisticas.setBounds(10, 311, 90, 87);
		contentPane.add(btnEstadisticas);
		
		JPanel paneIngreso = new JPanel();
		paneIngreso.setBorder(null);
		paneIngreso.setBackground(new Color(240, 128, 128));
		paneIngreso.setBounds(124, 11, 789, 387);
		contentPane.add(paneIngreso);
		paneIngreso.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 57, 374, 100);
		paneIngreso.add(scrollPane);
		
		dtmEsp = DTMIngreso(Especial);
		tblEspeciales = new JTable(dtmEsp);
		tblEspeciales.setFont(new Font("Tahoma", Font.PLAIN, 12));
		scrollPane.setViewportView(tblEspeciales);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 223, 374, 100);
		paneIngreso.add(scrollPane_1);
		
		dtmOro = DTMIngreso(Oro);
		tblOro = new JTable(dtmOro);
		tblOro.setFont(new Font("Tahoma", Font.PLAIN, 12));
		scrollPane_1.setViewportView(tblOro);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(405, 57, 374, 100);
		paneIngreso.add(scrollPane_2);
		
		dtmPla = DTMIngreso(Platino);
		tblPlatino = new JTable(dtmPla);
		tblPlatino.setFont(new Font("Tahoma", Font.PLAIN, 12));
		scrollPane_2.setViewportView(tblPlatino);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(405, 223, 374, 100);
		paneIngreso.add(scrollPane_3);
		
		dtmEco = DTMIngreso(Economico);
		tblEconomico = new JTable(dtmEco);
		tblEconomico.setFont(new Font("Tahoma", Font.PLAIN, 12));
		scrollPane_3.setViewportView(tblEconomico);
		
		JLabel label = new JLabel("INGRESO");
		label.setForeground(Color.BLACK);
		label.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		label.setBounds(332, 0, 191, 31);
		paneIngreso.add(label);
		
		
		JLabel lblEspecial = new JLabel("Especial");
		lblEspecial.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblEspecial.setBounds(147, 36, 97, 22);
		paneIngreso.add(lblEspecial);
		
		JLabel lblPlatino = new JLabel("Platino");
		lblPlatino.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblPlatino.setBounds(555, 36, 97, 22);
		paneIngreso.add(lblPlatino);
		
		JLabel lblOro = new JLabel("Oro");
		lblOro.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblOro.setBounds(147, 200, 97, 22);
		paneIngreso.add(lblOro);
		
		JLabel lblEconmico = new JLabel("Econ\u00F3mico");
		lblEconmico.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblEconmico.setBounds(555, 200, 143, 22);
		paneIngreso.add(lblEconmico);
		
		JButton btnAtender = new JButton("Atender");
		btnAtender.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Atender();
			}
		});
		btnAtender.setBackground(UIManager.getColor("Button.disabledShadow"));
		btnAtender.setForeground(SystemColor.desktop);
		btnAtender.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		btnAtender.setBounds(332, 345, 143, 31);
		paneIngreso.add(btnAtender);
		
		JPanel paneVIP = new JPanel();
		paneVIP.setBackground(new Color(255, 228, 196));
		paneVIP.setBounds(124, 409, 387, 264);
		contentPane.add(paneVIP);
		paneVIP.setLayout(null);
		
		JLabel lblVIP = new JLabel("V I P");
		lblVIP.setForeground(Color.BLACK);
		lblVIP.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		lblVIP.setBounds(171, 11, 191, 31);
		paneVIP.add(lblVIP);
		
		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(10, 69, 367, 148);
		paneVIP.add(scrollPane_4);
		
		Object[][] datosVIP = {{}};
		String[] titulosVIP = {"Puerta", "Vuelo", "Estado"};
		DefaultTableModel dtmVIP = new DefaultTableModel(datosVIP,titulosVIP);
		tblVIP = new JTable(dtmVIP);
		scrollPane_4.setViewportView(tblVIP);
		int indice = 0;
		while (indice < listaP.getSize()) {
			listaP.goToPos(indice);
			Puerta puerta = (Puerta) listaP.getElement();
			Object[] dato = {puerta.getCodigo(),puerta.getVuelo(),puerta.getEstado()};
			dtmVIP.addRow(dato);
			indice++;
		}
		
		JButton btnCerrarPuerta = new JButton("Cerrar Puerta");
		btnCerrarPuerta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int seleccionado = tblVIP.getSelectedRow();
				if ( seleccionado >= 0) {
					int num = (int) tblVIP.getValueAt(seleccionado, 0);
					for (int indice = 0; indice < listaP.getSize(); indice ++) {
						if (num == listaP.getElement().getCodigo()) {
							listaP.getElement().cerrar();
						}
						
					} tblVIP.setValueAt("cerrado", seleccionado, 2);
				}
			}
		});
		btnCerrarPuerta.setForeground(Color.BLACK);
		btnCerrarPuerta.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		btnCerrarPuerta.setBackground(UIManager.getColor("Button.background"));
		btnCerrarPuerta.setBounds(108, 228, 184, 31);
		paneVIP.add(btnCerrarPuerta);
		
		JPanel paneSalir = new JPanel();
		paneSalir.setBackground(new Color(255, 228, 196));
		paneSalir.setBounds(530, 410, 383, 263);
		contentPane.add(paneSalir);
		paneSalir.setLayout(null);
		
		JLabel lblSalida = new JLabel("SALIDA");
		lblSalida.setForeground(Color.BLACK);
		lblSalida.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		lblSalida.setBounds(118, 11, 191, 31);
		paneSalir.add(lblSalida);
		
		JScrollPane scrollPane_5 = new JScrollPane();
		scrollPane_5.setBounds(10, 67, 363, 156);
		paneSalir.add(scrollPane_5);
		
		Object[][] datosSal = {{}};
		String[] titulosSal = {"Servicio", "Pasajero", "Estado"};
		dtmSal = new DefaultTableModel(datosSal,titulosSal);
		tblSalida = new JTable(dtmSal);
		scrollPane_5.setViewportView(tblSalida);
		
		JButton btnSalir = new JButton("");
		btnEstadisticas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ventana1.main(null);
				setVisible(false);
				Comentario ventana = new Comentario();
				ventana.setVisible(true);
			}
		});
		btnSalir.setIcon(new ImageIcon(Ventana1.class.getResource("/Imagenes/salida.jpg")));
		btnSalir.setBounds(10, 409, 90, 87);
		contentPane.add(btnSalir);
		
	}
	
	public DefaultTableModel DTMIngreso (Servicio servicio) {
		Object[][] datos = {{}};
		String[] titulos = {"Pasajero", "Puerta", "Vuelo"};
		DefaultTableModel dtm = new DefaultTableModel(datos,titulos);
		if (metodo == "cola") {
			Nodo nodo = servicio.getCola().getFront().getNext();
			int size = servicio.getCola().getSize();
			while (size > 0) {
				Usuario usuario = (Usuario) nodo.getElement();
				Vuelo vuelo = usuario.getVuelo();
				Puerta puerta = vuelo.getPuerta();
				Object[] dato = {usuario.getNombre(),puerta.getCodigo(),vuelo.getCodigo()};
				dtm.addRow(dato);
				nodo = nodo.getNext();
				size--;
			}
		} else {
			for (int contador = 0;contador < servicio.getMonticulo().getSize();contador ++) {
				Usuario usuario = (Usuario) servicio.getMonticulo().getElement(contador);
				Vuelo vuelo = usuario.getVuelo();
				Puerta puerta = vuelo.getPuerta();
				Object[] dato = {usuario.getNombre(),puerta.getCodigo(),vuelo.getCodigo()};
				dtm.addRow(dato);
			}
		}
		return dtm;
	}

	public void Atender() {
		Usuario usuario = null;
		Servicio servicio;
		Calendar calendario = Calendar.getInstance();
		int segundos = calendario.get(Calendar.MINUTE) * 60 + calendario.get(Calendar.SECOND);
		if (metodo == "cola") {
			if (Especial.getCola().getSize() > 0) {
				servicio = Especial;
				dtmEsp.removeRow(0);
			} else if (Platino.getCola().getSize() > 0) {
				servicio = Platino;
				dtmPla.removeRow(0);
			} else if (Oro.getCola().getSize() > 0) {
				servicio = Oro;
				dtmOro.removeRow(0);
			} else {
				servicio = Economico;
				dtmEco.removeRow(0);
			} 
			usuario = (Usuario) servicio.getCola().extraer();
			servicio.sumarTiempo(segundos - usuario.getHoraEntrada());
			servicio.aumentarAtendidos();
			if (usuario.getVuelo().getPuerta().getEstado() == "abierto") {
				Salida.getCola().insertar(usuario);
			} else {
				JOptionPane.showMessageDialog(null, "La puerta esta cerrada","Error",JOptionPane.ERROR_MESSAGE);
			}
		} else {
			int prioridad = 0;
			if (Especial.getMonticulo().isVacio() == false) {
				prioridad = 100;
				servicio = Especial;
				dtmEsp.removeRow(0);
			} else if (Platino.getMonticulo().isVacio() == false) {
				prioridad = 200;
				servicio = Platino;
				dtmPla.removeRow(0);
			} else if (Oro.getMonticulo().isVacio() == false) {
				prioridad = 300;
				servicio = Oro;
				dtmOro.removeRow(0);
			} else {
				prioridad = 400;
				servicio = Economico;
				dtmEco.removeRow(0);
			} 
			prioridad += servicio.getMonticulo().getPrioridad();
			usuario = (Usuario) servicio.getMonticulo().extraer();
			servicio.sumarTiempo(segundos - usuario.getHoraEntrada());
			servicio.aumentarAtendidos();
			if (usuario.getVuelo().getPuerta().getEstado() == "abierto") {
				Salida.getMonticulo().insertar(prioridad, usuario);
			} else {
				JOptionPane.showMessageDialog(null, "La puerta esta cerrada","Error",JOptionPane.ERROR_MESSAGE);
			}
		}
		if (usuario.getVuelo().getPuerta().getEstado() == "abierto") {
			Object [] dato = {servicio.getTipo(),usuario.getNombre(),};
			dtmSal.addRow(dato);
		}
	}
}
