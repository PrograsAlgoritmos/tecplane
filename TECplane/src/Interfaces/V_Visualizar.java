package Interfaces;

import java.awt.EventQueue;
import java.awt.Dialog.ModalExclusionType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import Clases.Lista;
import Clases.Nodo;
import Clases.Puerta;
import Clases.Servicio;
import Clases.Usuario;
import java.awt.Color;
import javax.swing.JButton;

public class V_Visualizar extends JFrame{
	private Servicio Especial = V_config_inicial.servicioEsp;
	private Servicio Platino = V_config_inicial.servicioPla;
	private Servicio Oro = V_config_inicial.servicioOro;
	private Servicio Economico = V_config_inicial.servicioEco;
	private String metodo = V_config_inicial.metodoCola;
	private Lista listaP = V_config_inicial.lista_p;
	
	public V_Visualizar() {
		getContentPane().setBackground(new Color(218, 112, 214));
		setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(true);
		setTitle("Visualizar");
		setBounds(100, 100, 479, 462);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 28, 116, 352);
		getContentPane().add(scrollPane);
		
		JTextArea textAreaPuertas = new JTextArea();
		textAreaPuertas.setFont(new Font("Tahoma", Font.PLAIN, 14));
		scrollPane.setViewportView(textAreaPuertas);
		String textP = null;
		
		JLabel lblPuertas = new JLabel("Puertas");
		lblPuertas.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPuertas.setBounds(42, 12, 53, 17);
		getContentPane().add(lblPuertas);
		
		JLabel lblTipoCola = new JLabel();
		lblTipoCola.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTipoCola.setBounds(136, 12, 135, 23);
		lblTipoCola.setText("Tipo de metodo: " + metodo);
		getContentPane().add(lblTipoCola);
		
		JLabel lblCantidad = new JLabel("Cantidad de personas por cola");
		lblCantidad.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCantidad.setBounds(136, 38, 219, 23);
		getContentPane().add(lblCantidad);
		
		JLabel lblEspeciales = new JLabel("Especiales: ");
		lblEspeciales.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEspeciales.setBounds(158, 60, 87, 23);
		getContentPane().add(lblEspeciales);
		
		JLabel lblPlatino = new JLabel("Platino: ");
		lblPlatino.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPlatino.setBounds(158, 218, 87, 23);
		getContentPane().add(lblPlatino);
		
		JLabel lblOro = new JLabel("Oro: ");
		lblOro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblOro.setBounds(319, 218, 87, 23);
		getContentPane().add(lblOro);
		
		JLabel lblEconomico = new JLabel("Economico: ");
		lblEconomico.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEconomico.setBounds(319, 60, 87, 23);
		getContentPane().add(lblEconomico);
		
		JLabel lblSiguiente = new JLabel("Siguiente por atender: ");
		lblSiguiente.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSiguiente.setBounds(10, 391, 144, 20);
		getContentPane().add(lblSiguiente);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_1.setBounds(136, 84, 150, 135);
		getContentPane().add(scrollPane_1);
		
		JTextArea txtrAsientosEsp = new JTextArea();
		scrollPane_1.setViewportView(txtrAsientosEsp);
		txtrAsientosEsp.setFont(new Font("Tahoma", Font.PLAIN, 14));
		String textoEsp = "Asientos:\n";
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_2.setBounds(296, 84, 150, 135);
		getContentPane().add(scrollPane_2);
		
		JTextArea txtrAsientosEco = new JTextArea();
		scrollPane_2.setViewportView(txtrAsientosEco);
		txtrAsientosEco.setFont(new Font("Tahoma", Font.PLAIN, 14));
		String textoEco = "Asientos:\n";
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_3.setBounds(136, 245, 150, 135);
		getContentPane().add(scrollPane_3);
		
		JTextArea txtrAsientosPlat = new JTextArea();
		scrollPane_3.setViewportView(txtrAsientosPlat);
		txtrAsientosPlat.setFont(new Font("Tahoma", Font.PLAIN, 14));
		String textoPla = "Asientos:\n";
		
		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_4.setBounds(296, 245, 150, 135);
		getContentPane().add(scrollPane_4);
		
		JTextArea txtrAsientosOro = new JTextArea();
		scrollPane_4.setViewportView(txtrAsientosOro);
		txtrAsientosOro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		String textoOro = "Asientos:\n";
		
		int pos = 0;
		while (pos < listaP.getSize()) {
			listaP.goToPos(pos);
			Puerta puerta = (Puerta) listaP.getElement();
			String id = "Puerta " + puerta.getCodigo();
			textP += id + "\n";
			pos ++;
		}
		textAreaPuertas.setText(textP);
		
		if (metodo == "cola") {
			lblEspeciales.setText("Especiales: " + Especial.getCola().getSize());
			lblPlatino.setText("Platino: " + Platino.getCola().getSize());
			lblOro.setText("Oro: " + Oro.getCola().getSize());
			lblEconomico.setText("Economico: " + Economico.getCola().getSize());
		} else {
			lblEspeciales.setText("Especiales: " + Especial.getMonticulo().getSize());
			lblPlatino.setText("Platino: " + Platino.getMonticulo().getSize());
			lblOro.setText("Oro: " + Oro.getMonticulo().getSize());
			lblEconomico.setText("Economico: " + Economico.getMonticulo().getSize());
		}
		
		Servicio servicio;
		String texto;
		for (int indice = 1; indice < 5; indice++) {
			texto = "Asientos:\n";
			if (indice == 1) {
				servicio = Especial;
			} else if(indice == 2) {
				servicio = Platino;
			} else if(indice == 3) {
				servicio = Oro;
			} else {
				servicio = Economico;
			}
			if (metodo == "cola") {
				Nodo nodo = servicio.getCola().getFront().getNext();
				int size = servicio.getCola().getSize();
				while (size > 0) {
					Usuario usuario = (Usuario) nodo.getElement();
					texto += usuario.getAsiento() + "\n";
					nodo = nodo.getNext();
					size--;
				}
			} else {
				for (int contador = 0;contador < servicio.getMonticulo().getSize();contador ++) {
					Usuario usuario = (Usuario) servicio.getMonticulo().getElement(contador);
					texto += usuario.getAsiento();
				}
			}
			if (indice == 1) {
				textoEsp = texto;
			} else if(indice == 2) {
				textoPla = texto;
			} else if(indice == 3) {
				textoOro = texto;
			} else {
				textoEco = texto;
			}
		}
		txtrAsientosEsp.setText(textoEsp);
		txtrAsientosPlat.setText(textoPla);
		txtrAsientosEco.setText(textoEco);
		txtrAsientosOro.setText(textoOro);
		
		JButton btnCerrar = new JButton("Cerrar");
		btnCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_Visualizar.main(null);
				setVisible(false);
			}
		});
		btnCerrar.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnCerrar.setBounds(364, 392, 89, 23);
		getContentPane().add(btnCerrar);
		
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_Visualizar frame = new V_Visualizar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
