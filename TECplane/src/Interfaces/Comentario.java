package Interfaces;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Dialog.ModalExclusionType;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import Clases.Analizar;

public class Comentario extends JFrame{
	private JButton btnAceptar;
	public Comentario() {
		setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setTitle("Comentario");
		setBounds(100, 100, 441, 191);
		getContentPane().setLayout(null);
		
		JLabel lblPorFavorDenos = new JLabel("Por favor, denos su opinión acerca del servicio");
		lblPorFavorDenos.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPorFavorDenos.setBounds(10, 11, 333, 23);
		getContentPane().add(lblPorFavorDenos);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(20, 34, 393, 86);
		getContentPane().add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setFont(new Font("Tahoma", Font.PLAIN, 13));
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(329, 127, 95, 23);
		getContentPane().add(btnAceptar);
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Comentario.main(null);
				setVisible(false);
				Analizar analisis = null;
				try {
					analisis = new Analizar(textArea.getText());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				JOptionPane.showMessageDialog(null,analisis.getTexto(),"Analisis",JOptionPane.PLAIN_MESSAGE);
			}
		});
		
		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0, 434, 261);
		getContentPane().add(lblFondo);
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Comentario frame = new Comentario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
